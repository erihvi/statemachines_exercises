package com.evry.statemachine.common;

public abstract class StateMachineBase<E extends Enum, S extends StateEntityBase<E, ? extends StateMachineBase>> {

    protected S currentState = null;
    protected S previousState = null;

    public abstract void init();

    public void handle() throws Exception {
        currentState.handle();
    }

    protected void changeState() {
        if(previousState != null) {
            previousState.exiting();
        }
        if(currentState != null) {
            currentState.entering();
        }
        previousState = null;
    }

    public abstract E getCurrentState();
    public abstract void setState(E state);

}
