package com.evry.statemachine.utils;

import com.evry.statemachine.Utils.StatemachineUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

public class TestStatemachineUtils {

    @Test
    public void readFileTest() throws IOException {
        String foundPath = StatemachineUtils.getAbsoluteFilePath("FileToReadSimple.txt");
        String foundContent = StatemachineUtils.readContentsOfFile(foundPath);

        String expected = "someCompany|anonymous|2008-09-29|\n";

        Assert.assertEquals(expected, foundContent);
    }

    @Test
    public void readFileTestResourcesTest() throws IOException {
        String foundPath = StatemachineUtils.getAbsoluteFilePath("test.txt");
        String foundContent = StatemachineUtils.readContentsOfFile(foundPath);

        String expected = "someCompany|anonymous|2008-09-29|\n";

        Assert.assertEquals(expected, foundContent);
    }


}
