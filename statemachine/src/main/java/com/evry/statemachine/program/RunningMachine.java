package com.evry.statemachine.program;

import com.evry.statemachine.statemachine.ReaderStateMachine;

public class RunningMachine {

    public RunningMachine() {

    }

    public void run() {
        ReaderStateMachine stateParent = new ReaderStateMachine();
        stateParent.init();
        String path = "FileToReadSimple.txt";
        stateParent.setFilePath(path);
        try {
            stateParent.handle();
        } catch(Exception e) {

        }
    }
}
