package com.evry.statemachine;

import com.evry.statemachine.program.RunningMachine;

public class Main {

    public static void main(String... args){
        RunningMachine machine = new RunningMachine();
        machine.run();
    }
}
