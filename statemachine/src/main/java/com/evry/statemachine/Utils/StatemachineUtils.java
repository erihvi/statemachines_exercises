package com.evry.statemachine.Utils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by erik on 2017-04-28.
 */
public class StatemachineUtils {

    public static String getAbsoluteFilePath(String fileName) {
        String path = StatemachineUtils.class.getResource("/"+ fileName).getPath();
        path = path.replaceFirst("^/(.:/)", "$1");
        return path;
    }

    public static String readContentsOfFile(String path) throws IOException {
        StringBuffer buffer = new StringBuffer();
        Files.lines(Paths.get(path)).map(line -> line+"\n").forEach(line -> buffer.append(line));
        return buffer.toString();
    }
}
