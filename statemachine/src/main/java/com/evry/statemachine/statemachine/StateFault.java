package com.evry.statemachine.statemachine;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.rmi.runtime.Log;

public class StateFault extends ReaderStateEntityBase {

    private static Logger LOGGER = LoggerFactory.getLogger(StateFault.class);

    private Exception exception;

    public StateFault(ReaderStateMachine parent) {
        super(ReaderStateEnum.FAULT_STATE, parent);
    }

    @Override
    public void entering() {
        LOGGER.info("Entering StateFault");
    }

    @Override
    public void handle() throws Exception {
        LOGGER.error("Got exception: ", exception);
    }

    @Override
    public void exiting() {
        LOGGER.info("Exiting StateFault");
    }

    public Exception getException() {
        return exception;
    }

    public void setException(Exception exception) {
        this.exception = exception;
    }
}
