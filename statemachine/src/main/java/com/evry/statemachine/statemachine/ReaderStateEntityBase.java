package com.evry.statemachine.statemachine;

import com.evry.statemachine.common.StateEntityBase;


public abstract class ReaderStateEntityBase extends StateEntityBase<ReaderStateEnum, ReaderStateMachine> {

    public ReaderStateEntityBase(ReaderStateEnum stateEnum, ReaderStateMachine parent) {
        super(stateEnum, parent);
    }
}
