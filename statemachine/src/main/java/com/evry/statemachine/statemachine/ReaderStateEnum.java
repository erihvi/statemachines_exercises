package com.evry.statemachine.statemachine;


public enum ReaderStateEnum {
    INITIAL_STATE,
    READ_FILE_STATE,
    FAULT_STATE,
    PARSE_XML_STATE,
    PARSE_FLAT_FILE_STATE,
    VALID_STATE,
    INVALID_STATE,
    VALIDATING_STATE
}
