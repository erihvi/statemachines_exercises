package com.evry.statemachine.statemachine;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StateValid extends ReaderStateEntityBase {

    private static Logger LOGGER = LoggerFactory.getLogger(StateValid.class);

    public StateValid(ReaderStateMachine parent) {
        super(ReaderStateEnum.VALID_STATE, parent);
    }

    @Override
    public void entering() {
        LOGGER.info("Entering StateValid");
    }

    @Override
    public void handle() throws Exception {

    }

    @Override
    public void exiting() {
        LOGGER.info("Exiting StateValid");
    }
}
