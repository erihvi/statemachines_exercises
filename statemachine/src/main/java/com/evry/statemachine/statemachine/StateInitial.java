package com.evry.statemachine.statemachine;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StateInitial extends ReaderStateEntityBase {

    private static Logger LOGGER = LoggerFactory.getLogger(StateInitial.class);

    public StateInitial(ReaderStateMachine parent) {
        super(ReaderStateEnum.INITIAL_STATE, parent);
    }

    @Override
    public void entering() {
        LOGGER.info("Entering StateInitial");
    }

    @Override
    public void handle() throws Exception {
        if(parent.getFilePath() != null) {
            parent.setState(ReaderStateEnum.READ_FILE_STATE);
            parent.handle();
        } else {
            parent.setState(ReaderStateEnum.FAULT_STATE);
            parent.handle();
        }
    }

    @Override
    public void exiting() {
        LOGGER.info("Exiting StateInitial");
    }
}
