package com.evry.statemachine.statemachine;

import com.evry.statemachine.Utils.StatemachineUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StateReadFile extends ReaderStateEntityBase {

    private static Logger LOGGER = LoggerFactory.getLogger(StateReadFile.class);
    private String filePath = null;

    public StateReadFile(ReaderStateMachine parent) {
        super(ReaderStateEnum.READ_FILE_STATE, parent);
    }

    @Override
    public void entering() {
        LOGGER.info("Entering StateReadFile");
        filePath = parent.getFilePath();
    }

    @Override
    public void handle() throws Exception {
        String absoluteFilePath = StatemachineUtils.getAbsoluteFilePath(filePath);
        String content = StatemachineUtils.readContentsOfFile(absoluteFilePath);
        parent.setFileContent(content);

        if(content.startsWith("<?xml")|content.startsWith("<content")) {
            parent.setState(ReaderStateEnum.PARSE_XML_STATE);
        } else {
            parent.setState(ReaderStateEnum.PARSE_FLAT_FILE_STATE);
        }

        parent.handle();
    }

    @Override
    public void exiting() {
        LOGGER.info("Exiting StateReadFile");
        filePath = null;
    }
}
