package com.evry.statemachine.contentparsers;

import com.evry.statemachine.DataHolders.FileDataHolder;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class FlatFileContentParser {

    public static FileDataHolder getParsedContent(String content) throws Exception {
        String[] splitContent = content.trim().split("\\|");

        if (splitContent.length != 3) {
            throw new Exception("Incorrect content length");
        }
        return FileDataHolder.with()
                .company(splitContent[0])
                .startedBy(splitContent[1])
                .startedDate(LocalDate.parse(splitContent[2]))
                .build();
    }
}
